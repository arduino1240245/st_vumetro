

//Hecho por Federico Capdeville
//Probado con el Blue Pill (STM32F103C8T6)

//Vumetro de 5 leds, que cambia mediante el preset

#define LED1 PB12
#define LED2 PB13
#define LED3 PB14
#define LED4 PB15
#define LED5 PA8
#define PRESET PA0

int preset = 0;

void setup() 
{
  pinMode(LED1, OUTPUT);          //Coloco un led
  pinMode(LED2, OUTPUT);          //Coloco un led
  pinMode(LED3, OUTPUT);          //Coloco un led
  pinMode(LED4, OUTPUT);          //Coloco un led
  pinMode(LED5, OUTPUT);          //Coloco un led
}

void loop() 
{
  preset = analogRead (PRESET);   //Tomo el valor del preset

  if (preset < 819)
  {
    digitalWrite (LED1, HIGH);
    digitalWrite (LED2, LOW);
    digitalWrite (LED3, LOW);
    digitalWrite (LED4, LOW);
    digitalWrite (LED5, LOW);
  }

  if ((preset < 1638) && (preset > 819))
  {
    digitalWrite (LED1, HIGH);
    digitalWrite (LED2, HIGH);
    digitalWrite (LED3, LOW);
    digitalWrite (LED4, LOW);
    digitalWrite (LED5, LOW);
  }

  if ((preset < 2457) && (preset > 1638))
  {
    digitalWrite (LED1, HIGH);
    digitalWrite (LED2, HIGH);
    digitalWrite (LED3, HIGH);
    digitalWrite (LED4, LOW);
    digitalWrite (LED5, LOW);
  }

  if ((preset < 3276) && (preset > 2457))
  {
    digitalWrite (LED1, HIGH);
    digitalWrite (LED2, HIGH);
    digitalWrite (LED3, HIGH);
    digitalWrite (LED4, HIGH);
    digitalWrite (LED5, LOW);
  }

  if (preset > 3276)
  {
    digitalWrite (LED1, HIGH);
    digitalWrite (LED2, HIGH);
    digitalWrite (LED3, HIGH);
    digitalWrite (LED4, HIGH);
    digitalWrite (LED5, HIGH);
  }
  
}
